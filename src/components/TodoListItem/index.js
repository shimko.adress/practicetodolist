import React, {Component} from 'react';
import './index.css';

class TodoListItem extends Component {


    render() {

        const {name, isImportant, done} = this.props;

        let classNames = 'todo-list-item';
        if (done) {
            classNames += ' done';
        }

        if (isImportant) {
            classNames += ' important';
        }

        return (
            <span className={classNames}>
              <span
                  className="todo-list-item-label"
                  onClick={ this.props.onToggleDone }
              >
                {name}
              </span>

              <button type="button"
                className="btn btn-outline-success btn-sm float-right"
                onClick={ this.props.onToggleImportant }
              >
                <i className="fa fa-exclamation" />
              </button>

              <button type="button"
                className="btn btn-outline-danger btn-sm float-right"
                onClick={this.props.onDeleted}
              >
                <i className="fa fa-trash-o" />
              </button>
            </span>
        );
    }
}

export default TodoListItem;