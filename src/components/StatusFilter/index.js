import React, {Component} from 'react';

import './index.css';

class StatusFilter extends Component {
    /*
    state = {
        nameFilter: 'all'
    } 
    */
    onClick = (e) => {        
        // this.setState({nameFilter: e.target.value});
        this.props.onFilteredItems(e.target.value);        
    }
    
    render() {
        // const newClassName = "btn btn-outline-secondary";

        return (
            <div className="btn-group app-status-filter ">
                <button type="button"
                        className="btn btn-outline-secondary"
                        value = "all"
                        onClick={(e) => {this.onClick(e)}}                        
                        >
                            All
                </button>
                <button type="button"
                        className="btn btn-outline-secondary"
                        value = "active"
                        onClick={(e) => {this.onClick(e)}}
                        >
                            Active
                </button>
                <button type="button"
                        className="btn btn-outline-secondary"
                        value = "done"
                        onClick={(e) => {this.onClick(e)}}
                        >
                            Done
                </button>
            </div>
        );
    }
};

export default StatusFilter;
