import React, {Component} from 'react';
import './index.css';

class SearchPanel extends Component {

    state = {
        label: ''
    } 

    onSubmit = (e) => {
        e.preventDefault();
        this.props.onItemSearch(this.state.label);
        this.setState({
            label: ''
        });
    }

    render () {
        const searchText = 'Type here to search';
        return (
            <form
                className="d-flex app-search-panel"
                onSubmit={(e) => {this.onSubmit(e)}}
            >
                <input
                    type="text"
                    className="form-control"
                    placeholder={searchText}
                    onChange={(e) => {this.setState({label: e.target.value})}}
                    // value={this.state.label}
                ></input>
                <button
                    type="submit"
                    className="btn btn-outline-secondary"
                >
                    Search
                </button>
            </form>
         );  
    }
}

export default SearchPanel;