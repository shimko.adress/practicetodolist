import React from "react";
import SearchPanel from "../SearchPanel";
import TodoList from "../TodoList";
import './index.css';
import AppHeader from "../AppHeader";
import StatusFilter from "../StatusFilter";
import AddItem from "../AddItem";
import {Col, Row} from "antd";
import { Input } from 'antd';

class App extends React.Component {

    maxId = 200;

    state =  {
        todoData : [
            {name: 'Drink Coffee', isImportant: false, done: false, id: 1},
            {name: 'Learn React', isImportant: true, done: false, id: 2},
            {name: 'Build React App', isImportant: true, done: false, id: 3}
        ],
        search: '',
        filter: 'all',
        currentData: []
    };

    deleteItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const beforeArray = todoData.slice(0, idx);
            const afterArray = todoData.slice(idx+1);
            const newArray = [ ...beforeArray, ...afterArray ];

            return {
                todoData: newArray
            }
        });
    }

    onToggleProperty = (arr, id, propName) => {
        const idx = arr.findIndex((el) => el.id === id);
        const oldItem = arr[idx];
        const newItem = { ...oldItem, [propName]: !oldItem[propName]};

        const newArr = [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];

        return {
            todoData: newArr
        }
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => this.onToggleProperty(todoData, id, 'isImportant'));
    }

    onToggleDone = (id) => {
        this.setState(({todoData}) => this.onToggleProperty(todoData, id, 'done'));
    }

    addItem = (text) => {
        this.state.todoData.push();

        const newItem = {
            name: text,
            isImportant: false,
            done: false,
            id: this.maxId++
        };

        this.setState(({todoData}) => {
            const newArr = [
                ...todoData,
                newItem
            ];

            return {
                todoData: newArr
            }
        });
    }

    componentDidMount() {
        this.setState(({todoData}) => { 
            return {currentData: [...todoData]}; 
        });
    }
   
    searchItem = (searchText) => { 
        // console.log('searchText', searchText);
        this.setState(() => { return { search: searchText} });        
    }

    filteredItems = (status) => {
        // console.log('filter', status);
        this.setState(() => { return { filter: status} });
    }

    componentDidUpdate(prevProps, prevState) {
        const {todoData, search, filter} = this.state;
        // console.log('search:', search, 'filter:', filter) ;
        if (prevState.todoData !== todoData || prevState.filter !== filter || prevState.search !== search) {
            this.setState(({todoData}) => {
                const  newArray = [...todoData]
                    .filter(item => 
                        (item.name.toLowerCase().indexOf(search.toLowerCase()) !== -1)
                        && 
                        (filter === 'active' ? !item.done : (filter === 'done' ? item.done : true))
                    );
                
                return {
                    currentData: newArray                    
                }
            })                
        }
    }


    render() {

        const doneCount = this.state.todoData.filter((el) => el.done).length;
        const todoCount = this.state.todoData.length - doneCount;

        return (<div className="app-todo">
            <AppHeader toDo={todoCount} done={doneCount} />
            <SearchPanel onItemSearch={this.searchItem}/>
            <StatusFilter onFilteredItems={this.filteredItems}/>
            <TodoList
                todos={this.state.currentData}
                onDeleted={this.deleteItem}
                onToggleImportant={this.onToggleImportant}
                onToggleDone={this.onToggleDone}
            />
            <AddItem onItemAdded={this.addItem} />            

        </div>);
    }

}

export default App;